package com.mycompany.oxswing;

import com.mycompany.oxswing.Game;

public class MainProgram {

    public static void main(String[] args) {
        Game game = new Game();
        game.run();
    }
}
